Random Word Generator
=====================

This application makes random words. It's based on syllabels of English words.

I use the mhyph.txt file from Project Gutenberg, so thanks to them for that.

The file can be found here: https://www.gutenberg.org/files/3204/files/mhyph.txt

Usage
-----

You'll need to obtain the mhyph file, stick it in the same folder this file is
in then run ``process_mhyph.py`` file - it'll spit this horrid JSON thingy
which makes the random word generation a bit easier.

Comments
--------
As the title suggests, this is a pretty crap way of doing things I'm pretty sure
but hey, it works, and the words that come out of it are _okay_. Don't look to
this for any inspiration of how to code anything.
