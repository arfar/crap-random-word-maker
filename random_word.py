from process_mhyph import OUTPUT_FILENAME
import json
import random
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Random Word Generator')
    parser.add_argument('-n', '--num', type=int, help='Number of words', default=1)
    parser.add_argument('-l', '--length', type=int, help='Length of word(s) to create', default=4)
    results = parser.parse_args()

    d = {}
    with open(OUTPUT_FILENAME, 'r') as f:
        d = json.load(f)

    for num in range(results.num):
        random_word = ''
        for i in range(results.length):
            random_word += random.choice(d['positions and their syllables'][i])
        print(random_word)
